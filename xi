#!/bin/execlineb -WS0
# xi(1) simplified for my use case and translated into execline

backtick -nD "" branch {
	redirfd -w 2 /dev/null
	git symbolic-ref -q --short HEAD
}
backtick -in whoami { whoami }
multisubstitute {
	importas -u branch branch
	importas -u whoami whoami
}

define -s addrepo
"--repository=hostdir/binpkgs/${branch}
--repository=../hostdir/binpkgs/${branch}
--repository=../../hostdir/binpkgs/${branch}
--repository=hostdir/binpkgs/${branch}/nonfree
--repository=../hostdir/binpkgs/${branch}/nonfree
--repository=../../hostdir/binpkgs/${branch}/nonfree
--repository=hostdir/binpkgs/${branch}/debug
--repository=../hostdir/binpkgs/${branch}/debug
--repository=../../hostdir/binpkgs/${branch}/debug
--repository=hostdir/binpkgs
--repository=../hostdir/binpkgs
--repository=../../hostdir/binpkgs
--repository=hostdir/binpkgs/nonfree
--repository=../hostdir/binpkgs/nonfree
--repository=../../hostdir/binpkgs/nonfree
--repository=hostdir/binpkgs/debug
--repository=../hostdir/binpkgs/debug
--repository=../../hostdir/binpkgs/debug"

ifelse { test ${whoami} != root } {
	doas xbps-install ${addrepo} -S ${@}
} xbps-install ${addrepo} -S ${@}
