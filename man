#!/bin/execlineb -WS1
# man - somewhat more generic man wrapper
#
# BUGS
#	- man man

backtick -in progname { basename ${0} } importas -u progname progname

ifelse { test ${#} -gt 2 } {
	usgerr ${progname} [section] manpage
} #else
	if -nt {
		redirfd -w 2 /dev/null
		if -nt { mans ${@} }
			if -nt { man9 ${@} }
				if -nt { manv ${@} }
					manr ${@}
	} #then
		foreground { fdmove -c 1 2 echo ${progname}: ${@}: not found }
		exit 1
