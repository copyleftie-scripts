#!/bin/execlineb -W

backtick -in sv { defwhich sv }
multisubstitute {
	importas home home
	importas -u sv sv
	importas -u ctrl 1
}
shift
elgetpositionals -P1

# TODO
#	? doas ${sv} ${ctrl} ${@}, where ${@} is the paramaters but modified
#	? background { ${sv} ${ctrl} ${nonroots} } doas ${sv} ${ctrl} ${roots}
forx -p s { ${@} }
	importas -u s s
	ifelse { test -e ${home}/service/${s} } {
		${sv} ${ctrl} ${home}/service/${s}
	}
		doas ${sv} ${ctrl} ${s}